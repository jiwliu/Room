package com.Room.start.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.Room.start.pojo.Admin;

public interface AdminMapper {

	@Select("select * from admin where name = #{name}")
	public Admin login(@Param("name")String name);
	
	@Select("select * from admin where token = #{token}")
	public Admin haslogin(@Param("token")String token);

	@Update("update admin set token=#{token} where name = #{name}")
	public boolean updatetoken(@Param("name")String name,@Param("token")String token);
	
	@Update("update admin set token= NULL where token = #{token}")
	public boolean deletetoken(@Param("token")String token);

}

