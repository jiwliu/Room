package com.Room.start.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.Room.start.pojo.User;

public interface UserMapper {
	
	@Insert("insert into user(roomId,name,phone,sex,grade,school,`update`,`create`) values(#{roomId},#{name},#{phone},#{sex},#{grade},#{school},#{update},#{create})")
    public boolean addUser(User user);
	
	@Select("select * from user where isDel = 0 and roomId=#{roomId}")
	public List<User> selectUserByroomId(@Param("roomId") long roomId);
	
	@Select("update user set isPaid=#{isPaid} where userId=#{userId} and roomId = #{roomId}")
	public List<User> pay(@Param("isPaid") int isPaid,@Param("userId") long userId,@Param("roomId") long roomId);
}
