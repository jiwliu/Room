package com.Room.start.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.Room.start.pojo.Room;

public interface RoomMapper {
	
	@Select("select * from room where title = #{title}")
	public List<Room> selectByTitle(String title);
	
	@Select("select * from room where id = #{id}")
	public Room selectById(long id);
	
	@Select("select * from room where isDel = 0")
	public List<Room> selectAllRoom();

	@Select("select * from room order by isDel desc")
	public List<Room> selectExist();
	
	@Update("update room set image=#{image},title=#{title},capacity=#{capacity},price=#{price},discount=#{discount},isFree=#{isFree},`desc`=#{desc},remark=#{remark},`update`=#{update} where id=#{id}")
	public boolean updateRoomWithImg(Room room);
	

	@Update("update room set title=#{title},capacity=#{capacity},price=#{price},discount=#{discount},isFree=#{isFree},`desc`=#{desc},remark=#{remark},`update`=#{update} where id=#{id}")
	public boolean updateRoom(Room room);
	
	@Update("UPDATE room SET ordered = ordered + 1 WHERE id=#{id}")
	public boolean incOrderd(@Param("id") long id);
	
	@Insert("insert into room(image,title,`desc`,capacity,price,discount,isFree,remark,`update`,`create`) values(#{image},#{title},#{desc},#{capacity},#{price},#{discount},#{isFree},#{remark},#{update},#{create})")
    public boolean addRoom(Room room);
	
	@Update("update room set isDel= #{isDel} where id= #{id}")
    public boolean deleteRoom(@Param("id") long id, @Param("isDel") int isDel);

	@Update("update room set isFree= #{isFree} where id= #{id}")
    public boolean updateFree(@Param("id") long id, @Param("isFree") int isFree);

}
