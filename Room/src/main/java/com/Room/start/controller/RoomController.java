package com.Room.start.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.Room.start.Utils.RoomResult;
import com.Room.start.Utils.Utils;
import com.Room.start.pojo.Room;
import com.Room.start.service.RoomService;

@Controller
// @RestController //直接将所有返回都解析成json
public class RoomController {

	@Value("${FILEPATH}")
	private String FILEPATH;

	@Autowired
	private RoomService roomService;
	public static final String[] fileExt = { "jpg", "jpeg", "png", "bmp" };

	@RequestMapping(value = "/index")
	public String home(Model model) {
		// // 第几页和每页条数
		// PageHelper.startPage(1, 2);
		List<Room> rooms = roomService.selectAllRoom();
		model.addAttribute("rooms", rooms);
		return "index";
	}

	@RequestMapping(value = "/back/index")
	public String backhome(Model model) {
		// // 第几页和每页条数
		// PageHelper.startPage(1, 2);
		List<Room> rooms = roomService.selectAllRoom();
		model.addAttribute("rooms", rooms);
		return "backindex";
	}

	@RequestMapping(value = "/back/selectByTitle")
	@ResponseBody
	public List<Room> selectByTitle(String title) {
		return roomService.selectByTitle(title);
	}

	@RequestMapping(value = "/back/selectById")
	public Room selectById(long id) {
		return roomService.selectById(id);
	}

	@RequestMapping(value = "/back/selectAllRoom")
	public String selectAllRoom(Model model) {
		// // 第几页和每页条数
		// PageHelper.startPage(1, 2);
		List<Room> rooms = roomService.selectAllRoom();
		model.addAttribute("rooms", rooms);
		return "showRoom";
	}

	@RequestMapping("/back/selectExist")
	public List<Room> selectExist() {
		return roomService.selectExist();
	}

	@RequestMapping(value = "/back/showupdateRoom")
	public String showupdateRoom(Model model, long id) {
		Room room = roomService.selectById(id);
		model.addAttribute("room", room);
		return "updateRoom";
	}

	@RequestMapping("/back/showaddRoom")
	public String showaddRoom() {
		return "addRoom";
	}

	@RequestMapping(value = "/back/updateRoom")
	public String updateRoom(@RequestParam("file") MultipartFile file, Room room) {
		// 获取文件后缀名
		room.setUpdate(new Date());
		boolean result = false;
		if(file != null && !file.isEmpty()){
			String ext = getFileExt(file);
			// 判断文件是否符合文件后缀名规范
			if (isFileAllowed(ext)) {
					String image = Utils.getUUID() + "." + ext;
					room.setImage(image);
					try {
						Files.copy(file.getInputStream(), new File(FILEPATH + image).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						e.printStackTrace();
					}
			}else {
				room.setImage("default.jpg");
			}
			result = roomService.updateRoomWithImg(room);
		}else{
			result = roomService.updateRoom(room);
		}
		if (result) {
			return "success";
		}else
			return "error";
	}
	
	@RequestMapping(path = ("/image"), method = { RequestMethod.GET })
	@ResponseBody
	public void getImage(@RequestParam("name") String name,HttpServletResponse response){
		
		response.setContentType("image/jpeg");
		try {

			System.out.println(FILEPATH + name + "-------------");
			StreamUtils.copy(new FileInputStream(new File(FILEPATH + name)), response.getOutputStream());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

	@RequestMapping(path = ("/back/addRoom"), method = { RequestMethod.POST })
	public String addRoom(@RequestParam("file") MultipartFile file, Room room) {
		// 获取文件后缀名
		String ext = getFileExt(file);
		// 判断文件是否符合文件后缀名规范
		if (isFileAllowed(ext)) {
			if (!file.isEmpty()) {
				String image = Utils.getUUID() + "." + ext;
				room.setImage(image);
				System.out.println(FILEPATH + image + "!!!!!!!!!!!!!!");
				try {
					Files.copy(file.getInputStream(), new File(FILEPATH + image).toPath(), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			room.setImage("default.jpg");
		}
		room.setCreate(new Date());
		room.setUpdate(new Date());
		boolean result = roomService.addRoom(room);
		return "redirect:/back/index";
	}

	private String getFileExt(MultipartFile file) {
		String originalFilename = file.getOriginalFilename();
		int dotPos = originalFilename.lastIndexOf(".");
		String ext = originalFilename.substring(dotPos + 1);
		return ext;
	}

	private boolean isFileAllowed(String tmpExt) {
		for (String ext : fileExt) {
			if (tmpExt.equals(ext)) {
				return true;
			}
		}
		return false;
	}

	@RequestMapping(value = { "/back/deleteRoom" })
	@ResponseBody
	public RoomResult deleteRoom(long id, int isDel) {
		boolean result = roomService.deleteRoom(id, isDel);
		if (result == true)
			return RoomResult.ok();
		else
			return RoomResult.build(500, "服务器错误");
	}
}
