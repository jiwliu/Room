package com.Room.start.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.annotations.ResultMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Room.start.Utils.RoomResult;
import com.Room.start.Utils.Utils;
import com.Room.start.pojo.Admin;
import com.Room.start.service.AdminService;

@Controller
public class LoginController {

	@Autowired
	private AdminService adminservice;

	@RequestMapping("/tologin")
	public String tologin(){
		return "login";
	}
	
	@RequestMapping("/login")
	@ResponseBody
	public RoomResult login(HttpServletResponse response,String name, String password) {
		Admin admin = adminservice.login(name);
		if (admin == null) {
			return RoomResult.build(500, "登录失败,用户名或密码错误");
		}
		String rightPassword = admin.getPassword();
		String salt = admin.getSalt();
		String loginPassword = Utils.MD5(password + salt);
		if(rightPassword.equals(loginPassword)){
			String token = Utils.getUUID();
			Cookie cookie = new Cookie("token",token);
			cookie.setMaxAge(60*60*24);//设置一天有效期
			cookie.setPath("/");//可在同一应用服务器内共享方法
			response.addCookie(cookie);
			//不使用session，这里在数据库保存
			adminservice.updatetoken(admin.getName(), token);
			return RoomResult.ok();
			
		}else{
			return RoomResult.build(500, "登录失败,用户名或密码错误");	
		}
	}
	@RequestMapping("/loginout")
	@ResponseBody
	public RoomResult loginout(@CookieValue("token") String token,HttpServletResponse response,String name, String password) {
		boolean result = adminservice.deletetoken(token);
		if (result) {
			return RoomResult.ok();
		}else {
			return RoomResult.build(500, "注销失败");
		}
	}
}
