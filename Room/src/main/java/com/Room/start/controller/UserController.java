package com.Room.start.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Room.start.Utils.RoomResult;
import com.Room.start.pojo.User;
import com.Room.start.service.UserService;

@Controller
//@RestController //直接将所有返回都解析成json
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/addUser")
	@ResponseBody
	public RoomResult addUser(User user) {
//		// 第几页和每页条数
//		PageHelper.startPage(1, 2);
		user.setUpdate(new Date());
		user.setCreate(new Date());
		boolean addRoom = userService.addUser(user);
		if (addRoom) {
			return RoomResult.ok();
		}else
			return RoomResult.build(500, "服务器错误");
	}
	
	@RequestMapping(value = "/back/selectUserByroomId")
	public String selectUserByroomId(Model model,long roomId) {
		List<User> users =userService.selectUserByroomId(roomId);
		model.addAttribute("users", users);
		model.addAttribute("roomId", roomId);
		return "showUsers";
	}
	
	@RequestMapping(value = "/showaddUser")
	public String showaddUser(Model model,long roomId) {
		model.addAttribute("roomId", roomId);
		return "addUser";
	}
}
