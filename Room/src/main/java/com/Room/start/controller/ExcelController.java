package com.Room.start.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.Room.start.pojo.Room;
import com.Room.start.pojo.User;
import com.Room.start.service.RoomService;
import com.Room.start.service.UserService;

@Controller
public class ExcelController {
	@Autowired
	private UserService userService;

	@Autowired
	private RoomService roomService;
	
	@RequestMapping("/back/download")
    public void downstudents(HttpServletRequest request, HttpServletResponse response,long roomId)
    {  
   
        String[] headers = { "序号", "姓名", "性别", "学校", "年级", "手机号", "是否付款","预定时间"};//导出的Excel头部，这个要根据自己项目改一下
        Room room = roomService.selectById(roomId);
        List<User> users =userService.selectUserByroomId(roomId);
        // 声明一个工作薄
        HSSFWorkbook workbook = new HSSFWorkbook();
        // 生成一个表格
        HSSFSheet sheet = workbook.createSheet();
        // 设置表格默认列宽度为15个字节
        sheet.setDefaultColumnWidth((short) 18);
        HSSFRow row = sheet.createRow(0);
        for (short i = 0; i < headers.length; i++) {
            HSSFCell cell = row.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);
            cell.setCellValue(text);
        }
        //遍历集合数据，产生数据行
        Iterator<User> it = users.iterator();
        int index = 1;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        while (it.hasNext()) {
            row = sheet.createRow(index);
            User user = (User) it.next();
            row.createCell(0).setCellValue(index);
            row.createCell(1).setCellValue(user.getName());
            row.createCell(2).setCellValue(user.getSex());
            row.createCell(3).setCellValue(user.getSchool());
            row.createCell(4).setCellValue(user.getGrade());
            row.createCell(5).setCellValue(user.getPhone());
            row.createCell(6).setCellValue(user.getIsPaid() == 0?"未付款":"已付款");
            row.createCell(7).setCellValue(simpleDateFormat.format(user.getCreate()));
            index++;
        }
        try {
        	handleEncode(request, response, room);
			response.flushBuffer();
			workbook.write(response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

	private void handleEncode(HttpServletRequest request, HttpServletResponse response, Room room)
			throws UnsupportedEncodingException {
		String agent = request.getHeader("USER-AGENT").toLowerCase();
        String fileName = room.getTitle()+"用户预定信息";
        String codedFileName = null;
		try {
			codedFileName = java.net.URLEncoder.encode(fileName, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		response.setContentType("application/octet-stream");
		if (agent.contains("firefox")) {
			response.setCharacterEncoding("utf-8");
			response.setHeader("content-disposition", "attachment;filename=" + new String(fileName.getBytes(), "ISO8859-1") + ".xls");
		} else {
			response.setHeader("content-disposition", "attachment;filename=" + codedFileName + ".xls");
		}
	}
}
