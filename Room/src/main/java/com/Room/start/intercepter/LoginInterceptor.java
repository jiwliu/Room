package com.Room.start.intercepter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.Room.start.mapper.AdminMapper;
import com.Room.start.pojo.Admin;

@Component
public class LoginInterceptor implements HandlerInterceptor {
	@Autowired
	private AdminMapper adminMapper;

	// 在执行目标方法之前执行
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		Cookie[] cookies = request.getCookies();
		String token = null;
		if (cookies != null && cookies.length > 0) {
			for (Cookie cookie : cookies) {
				if ("token".equals(cookie.getName())) {
					token = cookie.getValue();
				}
			}
		}
		if (token == null || token.isEmpty()) {
			response.sendRedirect("/tologin");
			return false;
		}
		Admin admin = adminMapper.haslogin(token);
		if (admin == null) {
			response.sendRedirect("/tologin");
			return false;
		} else {
			return true;
		}
	}

	// 执行目标方法之后还未返回结果之前执行
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	// 在请求已经返回之后执行
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

	}

}
