package com.Room.start.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Room.start.mapper.AdminMapper;
import com.Room.start.pojo.Admin;

@Service
public class AdminService {

	@Autowired
	private AdminMapper adminMapper;

	public Admin login(String name) {
		return adminMapper.login(name);
	}
	
	public Admin haslogin(String token){
		return adminMapper.haslogin(token);
	}

	public boolean updatetoken(String name,String token){
		return adminMapper.updatetoken(name, token);
	}
	
	public boolean deletetoken(String token){
		return adminMapper.deletetoken(token);
	}


}
