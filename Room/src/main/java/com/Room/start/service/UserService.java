package com.Room.start.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Room.start.mapper.RoomMapper;
import com.Room.start.mapper.UserMapper;
import com.Room.start.pojo.User;

@Service
public class UserService {

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private RoomMapper roomMapper;
	@Transactional
	public boolean addUser(User user) {
		boolean addResult = userMapper.addUser(user);
		if(addResult == true){
			if(roomMapper.incOrderd(user.getRoomId()) == true)
				return true;
			else
				return false;
					
		}else
			return false;
	}

	public List<User> selectUserByroomId(long roomId) {
		return userMapper.selectUserByroomId(roomId);
	}

}
