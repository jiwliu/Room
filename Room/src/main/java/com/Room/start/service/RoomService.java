package com.Room.start.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Room.start.mapper.RoomMapper;
import com.Room.start.pojo.Room;

@Service
public class RoomService {

	@Autowired
	private RoomMapper roomMapper;
	
	public List<Room> selectByTitle(String title){
		return roomMapper.selectByTitle(title);
	}
	
	public Room selectById(long id){
		return roomMapper.selectById(id);
	}
	
	public List<Room> selectAllRoom(){
		return roomMapper.selectAllRoom();
	}

	public List<Room> selectExist(){
		return roomMapper.selectExist();
	}
    public boolean updateRoom(Room room){
    	return roomMapper.updateRoom(room);
    }
    public boolean updateRoomWithImg(Room room) {
    	return roomMapper.updateRoomWithImg(room);
	}
    public boolean addRoom(Room room){
    	return roomMapper.addRoom(room);
    }
    public boolean deleteRoom(long id,int isDel){
    	return roomMapper.deleteRoom(id,isDel);
    }
	
}
