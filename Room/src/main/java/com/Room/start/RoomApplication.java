package com.Room.start;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@MapperScan("com.Room.start.mapper")
public class RoomApplication {

	public static void main(String[] args) {
		//所有代码必须在该类的同级目录/下级目录下
		SpringApplication.run(RoomApplication.class, args);
	}
}
