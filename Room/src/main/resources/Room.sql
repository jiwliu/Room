/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.7.12-log : Database - room
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE IF NOT EXISTS Room DEFAULT CHARSET utf8 COLLATE utf8_general_ci; 

USE `Room`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `isDel` int(11) DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  `update` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `admin` */

insert  into `admin`(`id`,`name`,`password`,`salt`,`isDel`,`remark`,`update`,`create`,`token`) values (1,'root','B21BCFC33E5341DBD9E88CA9F0337D7C','123456',0,'唯一管理员','2017-10-31 15:42:21','2017-10-31 15:42:21',NULL);

/*Table structure for table `room` */

DROP TABLE IF EXISTS `room`;

CREATE TABLE `room` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `ordered` int(11) DEFAULT '0',
  `price` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `capacity` int(5) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `isFree` int(11) DEFAULT '0',
  `isDel` int(11) DEFAULT '0',
  `create` datetime DEFAULT NULL,
  `update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

/*Data for the table `room` */
/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roomId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `school` varchar(255) DEFAULT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `isPaid` int(11) DEFAULT '0',
  `isDel` int(11) DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  `update` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8